# Session Class #

A easy and simple to use session manager.

### Usage ###

*coming soon*

### Database ###


```
#!sql
CREATE TABLE `Session` (
	`id` text NOT NULL,
	`data` text NOT NULL,
	`lastUpdateTimestamp` int(10) unsigned NOT NULL,
	PRIMARY KEY (`id`(20))
) ENGINE=InnoDB DEFAULT CHARSET=utf8; 
```


### composer.json ###

First, you've to add this to your [`composer.json`](http://getcomposer.org/) dependencies:

```
#!json
"require": {
	"shirerom/sessionclass": ">=1.0"
}
```

and run

```
#!bash
composer update
```

### License ###

The MIT License (MIT)

Copyright (c) 2015 Johannes Bauer

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.