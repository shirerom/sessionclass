<?php

/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2015 Johannes Bauer <mail@bauerjohannes.de>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE. 
 */


/**

Database

CREATE TABLE `Session` (
  `id` text NOT NULL,
  `data` text NOT NULL,
  `lastUpdateTimestamp` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`(20))
) ENGINE=InnoDB DEFAULT CHARSET=utf8; 


Composer

{
	"require: [
		shirerom/databaseclass: ">=1.0"
	]
}

 */

/**
 * A simple and easy to use Session Manager
 */
class Session {

	const LIFETIME_MAX = 1440;

	var $lifetimeMax = 0;
	
	var $db;


	/**
	 * Constructor
	 * 
	 * @param Database $db Database object
	 */
	public function __construct(Database $db) {

		$this->db = $db;
		
		session_set_save_handler (
			array($this, "open"),
			array($this, "close"),
			array($this, "read"),
			array($this, "write"),
			array($this, "destroy"),
			array($this, "gc")
		);

		register_shutdown_function('session_write_close');
		
		session_start();
	}

	/**
	 *
	 * @param string $path
	 * @param string $name
	 * @return boolean
	 */
	public function open($path, $name) {
		return true;
	}

	/**
	 *
	 * @return boolean
	 */
	public function close() {

		$this->gc();

		return true;
	}

	/**
	 *
	 * @param string $id
	 * @return string
	 */
	public function read($id) {

		try {

			$session = $this->db->execute("
				SELECT
						data
					FROM Session
					WHERE id = '{$this->db->escape_string($id)}'
			")->getRow();

			if(is_array($session)) {
				return $session["data"];
			}
		}

		catch(DatabaseException $e) {
			// nix
		}

		return "";
	}

	/**
	 *
	 * @param string $id
	 * @param string $data
	 * @return boolean
	 */
	public function write($id, $data) {

		return $this->db->execute("
			REPLACE INTO Session
				SET
					id = '{$this->db->escape_string($id)}',
					data = '{$this->db->escape_string($data)}',
					lastUpdateTimestamp = ".time()."
		");
	}

	/**
	 *
	 * @param string $id
	 * @return boolean
	 */
	public function destroy($id) {

		return $this->db->execute("
			DELETE FROM Session
				WHERE id = '{$this->db->escape_string($id)}'
		");
	}

	/**
	 *
	 */
	public function gc() {

		if ($this->lifetimeMax > 0) {
			$sessionLife = time() - $this->lifetimeMax;
		}
		
		else {
			$sessionLife = time() - self::LIFETIME_MAX;
		}

		return $this->db->execute("
			DELETE FROM Session
				WHERE lastUpdateTimestamp < '{$sessionLife}'
		");
	}
}

?>
